module.exports = {

    entry: {
        books: './component/book/all/books-page.js',
        detailbooks: './component/book/detail/detailBook.js',
        authors: './component/author/all/authors.js',
        detailAuthor: './component/author/detail/detailAuthor.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist'
    },

    module: {
        loaders: [
            { test: /\.js$/, loader: "babel-loader" }
        ]
    }
};