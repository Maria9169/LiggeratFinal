/**
 * Created by Maria on 21.07.2017.
 */

import {getBooks} from '../service';
books();
export default function books() {
    let buttonSearch = document.getElementById("search_button");


    let tmp = [];

    let promise = new Promise((resolve, reject) => {
        resolve(getBooks());
    });

    promise
        .then(
            result => {
                renderbooks(result);
                tmp = result;
            },
            error => {
                console.log("Rejected: " + error);
            }
        );

    $.get("http://localhost:3000/books", function (data) {
        tmp = data;
        renderbooks(data);

        //createProfile(data);
    });
    let books = document.querySelector('.books-grid');
    let maxIndex = 0;

    function renderbooks(data) {
        for (var i = maxIndex; i < data.length; i++) {
            if (i < data.length)
            /*console.log(data[i]);*/
                renderItem(data[i]);
        }
    }

    function renderItem(item) {
        var container = document.createElement('div');
        var img = document.createElement('img');
        var title = document.createElement('a');
        var genre = document.createElement('p');
        var data = document.createElement('p');
        var about = document.createElement('p');
        var price= document.createElement('p');
        var avail= document.createElement('p');
        var buy = document.createElement('button');

        container.classList.add('data-grid');

        title.innerHTML = item.title;
        title.setAttribute('href', '../detail/detailBook.html?' + item._id);
        img.setAttribute('src', item.picture);
        img.classList.add('image');
        genre.innerHTML = item.genre;
        data.innerHTML = item.registered;
        about.innerHTML = item.about;
        about.classList.add('textAbout');
        buy.innerText = 'Купить';
        buy.classList.add('buyBook');
        buy.setAttribute('href','../../../component/shop/shop.html');
        buy.setAttribute('data-id',item._id);
        buy.getAttribute('data-id');
        buy.classList.add('buy');
        buy.addEventListener('click',goToShop);
        price.innerHTML=item.price;
        avail.innerHTML=item.Availability;

        container.appendChild(img);
        container.appendChild(title);
        container.appendChild(genre);
        container.appendChild(data);
        container.appendChild(about);
        container.appendChild(price);
        container.appendChild(avail);
        container.appendChild(buy);

        books.appendChild(container);

    }

    function goToShop(e) {
        let id=e.target.getAttribute('data-id');
        let shopArr =JSON.parse(localStorage.getItem('Shop')) || [];

        shopArr.push(id);
        console.log(shopArr);


            localStorage.setItem('Shop',JSON.stringify(shopArr));
    }

    function search(data, field, template) {
        var resultArr = [];
        var localTemplate = template.toLowerCase();
        data.forEach((element) => {
            let currentElement = element[field].toLowerCase();
            if (currentElement.indexOf(localTemplate) !== -1) {
                resultArr.push(element);
            }

        });
        console.log(resultArr);
        return resultArr;

    }

    buttonSearch.onclick = function () {
        var title = document.getElementById("find").value;
        //console.log(title);

        searchResult(search(tmp, 'title', title));

    }
    function searchResult(resultArr) {
        console.log(resultArr.length);
        let node = books.childNodes.length;

        while (books.firstChild) {
            books.removeChild(books.firstChild);
        }
        if (resultArr.length === 0) {
            var messageerror = document.createElement('p');
            messageerror.innerText = 'Не найдено';
            books.appendChild(messageerror);
        }
        else {
            renderbooks(resultArr);
        }
    }

    function createFilters(arrayFilters) {
        const filtersBlock = document.querySelector('.filters');
        arrayFilters.forEach((filter) => {
            let ul=document.createElement('ul');
            let btn = document.createElement('li');
            btn.innerHTML = filter;

            btn.addEventListener('click', filterData);
            ul.appendChild(btn);
            filtersBlock.appendChild(ul);
        })
    }

    function filterData() {
        let value = this.innerHTML;
        console.log(value);
        let container = document.querySelector('.books-grid');

        let newArray = tmp.filter((book) => {
            return value === book.genre;
        });
        container.innerHTML = '';
        renderbooks(newArray);
    }

    createFilters(['Детская литература', 'Детективы','Классика','Зарубежная литература','Драма']);
    function filteravailable(arr) {
        const  filterA= document.querySelector('.filtersAvailable');
        arr.forEach((filter) => {
            let ul=document.createElement('ul');
            let btn = document.createElement('li');
            btn.innerHTML = filter;

            btn.addEventListener('click', filterDataAvaileble);
            ul.appendChild(btn);
            filterA.appendChild(ul);
        })
    }
     function filterDataAvaileble(){
         let value = this.innerHTML;
         console.log(value);
         let container = document.querySelector('.books-grid');

         let newArray = tmp.filter((book) => {
             return value === book.Availability;
         });
         container.innerHTML = '';
         renderbooks(newArray);
     }
filteravailable(['В наличии','Нет в наличии']);

    let priceSort=document.querySelector('.filterPrice');
     priceSort.onclick =function () {
         var minValue = document.getElementById('minPrice').value;
         var maxValue = document.getElementById('maxPrice').value;
         console.log(minValue,maxValue);
        sort1(tmp,minValue,maxValue);
     }

    let container = document.querySelector('.books-grid');
     function sort1(tmp,min,max) {
         console.log(min);
         container.innerHTML = '';
         /*let newArr=[];
         tmp.forEach((element) => {
             if(element.price>1 && element.price<=5)
             {
                 console.log(element);
                 container.innerHTML = '';
                 renderbooks(element);
             }
         })*/
         for(let i=0;i<tmp.length;i++)
         {
             if(tmp[i].price>min && tmp[i].price<=max)
             {

                 console.log(tmp[i]);
                 renderItem(tmp[i]);

             }

         }
     }

}




