import {detailBookService} from '../service';

detailBooks();

//console.log('hello ', window);
export default function detailBooks() {
    var tmp=[];

    // const id = '59732ab83f8f7ac022a4f2f6';
    const id = window.location.search.slice(1);
    // console.log('this - ', this);
    let promise = new Promise((resolve, reject) => {
        resolve(detailBookService(id));
    });

    promise
        .then(
            result => {
                CreateDetailBook(result);
                tmp = result;
            },
            error => {
                console.log("Rejected: " + error);
            }
        );

    function CreateDetailBook(item) {
        var detailBook = document.querySelector('.detailBook');
        let container = document.createElement('div');
        let title = document.createElement('h1');
        let img = document.createElement('img');
        let genre = document.createElement('p');
        let data = document.createElement('p');
        let about = document.createElement('p');
        let avail= document.createElement('p');
        let price= document.createElement('p');
        let author= document.createElement('p');
        let publish= document.createElement('p');


        title.innerHTML = item.title;
        img.setAttribute('src', item.picture);
        genre.innerHTML = item.genre;
        data.innerHTML = item.registered;
        about.innerHTML = item.about;
        avail.innerHTML=item.Availability;
        price.innerHTML=item.price;
        author.innerHTML=item.author;
        publish.innerHTML=item.Publishing_house;

        container.appendChild(img);
        container.appendChild(title);
        container.appendChild(genre);
        container.appendChild(data);
        container.appendChild(author);
        container.appendChild(publish);
        container.appendChild(avail);
        container.appendChild(price);
        container.appendChild(about);


        detailBook.appendChild(container);


    }
}