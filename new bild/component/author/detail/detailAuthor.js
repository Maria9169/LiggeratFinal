const id=this.location.search.slice(1);

$.get( "http://localhost:3000/authors/"+id, function( data ) {
    console.log(data);
    createProfile(data);
});
function createProfile(data) {
	var authors = document.querySelector('.detailAuthor');
	var container = document.createElement('div');
    var avatar = document.createElement('img');
    var firstName = document.createElement('h1');
    var about = document.createElement('p');
    var genre = document.createElement('p');
    var registered = document.createElement('p');

    firstName.innerText = data.firstname;
    about.innerText = data.about;
    genre.innerText = data.genre;
    registered.innerText = data.registered;

    avatar.setAttribute('src',data.avatar);
    container.appendChild(avatar);
    container.appendChild(firstName);
    container.appendChild(about);
    container.appendChild(genre);
    container.appendChild(registered);

    authors.appendChild(container);
}