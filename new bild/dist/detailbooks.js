/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = getBooks;
/* harmony export (immutable) */ __webpack_exports__["a"] = detailBookService;
/**
 * Created by Maria on 26.07.2017.
 */

function getBooks() {
    return $.get("http://localhost:3000/books", function (data) {
        console.log(data);
        return data;
    });
}
function detailBookService(id) {
    return $.get("http://localhost:3000/books/" + id, function (data) {
        console.log('dat data - ', data);
        return data;
    });
}

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["default"] = detailBooks;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service__ = __webpack_require__(0);


detailBooks();

//console.log('hello ', window);
function detailBooks() {
    var tmp = [];

    // const id = '59732ab83f8f7ac022a4f2f6';
    const id = window.location.search.slice(1);
    // console.log('this - ', this);
    let promise = new Promise((resolve, reject) => {
        resolve(Object(__WEBPACK_IMPORTED_MODULE_0__service__["a" /* detailBookService */])(id));
    });

    promise.then(result => {
        CreateDetailBook(result);
        tmp = result;
    }, error => {
        console.log("Rejected: " + error);
    });

    function CreateDetailBook(item) {
        var detailBook = document.querySelector('.detailBook');
        let container = document.createElement('div');
        let title = document.createElement('h1');
        let img = document.createElement('img');
        let genre = document.createElement('p');
        let data = document.createElement('p');
        let about = document.createElement('p');
        let avail = document.createElement('p');
        let price = document.createElement('p');
        let author = document.createElement('p');
        let publish = document.createElement('p');

        title.innerHTML = item.title;
        img.setAttribute('src', item.picture);
        genre.innerHTML = item.genre;
        data.innerHTML = item.registered;
        about.innerHTML = item.about;
        avail.innerHTML = item.Availability;
        price.innerHTML = item.price;
        author.innerHTML = item.author;
        publish.innerHTML = item.Publishing_house;

        container.appendChild(img);
        container.appendChild(title);
        container.appendChild(genre);
        container.appendChild(data);
        container.appendChild(author);
        container.appendChild(publish);
        container.appendChild(avail);
        container.appendChild(price);
        container.appendChild(about);

        detailBook.appendChild(container);
    }
}

/***/ })
/******/ ]);