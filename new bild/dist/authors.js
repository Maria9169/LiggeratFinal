/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ 3:
/***/ (function(module, exports) {

var tmp = [];
$.get("http://localhost:3000/authors", function (data) {
    tmp = data;
    renderbooks(data);

    // createProfile(data);
});
var authors = document.querySelector('.authors');
var maxIndex = 0;
function renderbooks(data) {
    for (var i = maxIndex; i < data.length; i++) {
        if (i < data.length) console.log(data[i]);
        renderItem(data[i]);
    }
}

function renderItem(item) {
    var container = document.createElement('div');
    var containerForImg = document.createElement('div');
    var avatar = document.createElement('img');
    var firstName = document.createElement('a');
    var about = document.createElement('p');
    var genre = document.createElement('p');
    var registered = document.createElement('p');

    firstName.setAttribute('href', '../detail/detailAuthor.html?' + item._id);
    firstName.innerText = item.firstname;
    about.innerText = item.about;
    genre.innerText = item.genre;
    registered.innerText = item.registered;

    container.classList.add('authorsGrid');
    container.setAttribute('data-id', item.index);
    avatar.setAttribute('src', item.avatar);
    containerForImg.appendChild(avatar);
    container.appendChild(containerForImg);
    container.appendChild(firstName);
    container.appendChild(about);
    container.appendChild(genre);
    container.appendChild(registered);

    authors.appendChild(container);
}

function search(data, field, template) {
    var resultArr = [];
    var localTemplate = template.toLowerCase();
    data.forEach(element => {
        let currentElement = element[field].toLowerCase();
        if (currentElement.indexOf(localTemplate) !== -1) {
            resultArr.push(element);
        }
    });
    console.log(resultArr);
    return resultArr;
}

buttonSearch.onclick = function () {
    var firstname = document.getElementById("find").value;
    searchResult(search(tmp, 'firstname', firstname));
};

function searchResult(resultArr) {
    console.log(resultArr.length);

    while (authors.firstChild) {
        authors.removeChild(authors.firstChild);
    }

    if (resultArr.length === 0) {
        var messageerror = document.createElement('p');
        messageerror.innerText = 'Не найдено';
        authors.appendChild(messageerror);
    } else {
        renderbooks(resultArr);
    }
}

function createFilters(arrayFilters) {
    const filtersBlock = document.querySelector('.filters');
    arrayFilters.forEach(filter => {
        let btn = document.createElement('button');
        btn.innerHTML = filter;
        btn.addEventListener('click', filterData);
        filtersBlock.appendChild(btn);
    });
}

function filterData() {
    let value = this.innerHTML;
    console.log(value);
    let container = document.querySelector('.authors');

    let newArray = tmp.filter(book => {
        return value === book.genre;
    });
    container.innerHTML = '';
    renderbooks(newArray);
}

createFilters(['Военное дело', 'Документальная литература']);

/***/ })

/******/ });